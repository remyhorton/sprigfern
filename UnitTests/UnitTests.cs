using System;
using NUnit.Framework;
using SprigFern;

namespace UnitTests
{
[TestFixture]
public class TestNumberConstraint
	{
	[Test]
	public void InitialSet()
		{
		NumberConstraint numbers = new NumberConstraint(4);
		Assert.AreEqual(0, numbers.certainValue());
		Assert.AreEqual(1, numbers.lowest());
		Assert.AreEqual(4, numbers.highest());
		CollectionAssert.AreEquivalent(new uint[]{1,2,3,4}, numbers.possible());
		}
	[Test]
	public void RemoveMultipleTimes()
		{
		NumberConstraint numbers = new NumberConstraint(2);
		Assert.IsTrue(numbers.remove(1));
		Assert.IsFalse(numbers.remove(1));
		}

	[Test]
	public void RemoveMiddle()
		{
		NumberConstraint numbers = new NumberConstraint(4);
		numbers.remove(3);
		Assert.AreEqual(1, numbers.lowest());
		Assert.AreEqual(4, numbers.highest());
		numbers.remove(2);
		Assert.AreEqual(1, numbers.lowest());
		Assert.AreEqual(4, numbers.highest());
		Assert.AreEqual(0, numbers.certainValue());
		CollectionAssert.AreEquivalent(new uint[]{1,4}, numbers.possible());
		}
	[Test]
	public void RemoveLow()
		{
		NumberConstraint numbers = new NumberConstraint(4);
		numbers.remove(1);
		Assert.AreEqual(2, numbers.lowest());
		Assert.AreEqual(4, numbers.highest());
		CollectionAssert.AreEquivalent(new uint[]{2,3,4}, numbers.possible());
		}
	[Test]
	public void RemoveHigh()
		{
		NumberConstraint numbers = new NumberConstraint(4);
		numbers.remove(4);
		Assert.AreEqual(1, numbers.lowest());
		Assert.AreEqual(3, numbers.highest());
		CollectionAssert.AreEquivalent(new uint[]{1,2,3}, numbers.possible());
		}
	[Test]
	public void Set()
		{
		NumberConstraint numbers = new NumberConstraint(4);
		numbers.set(3);
		Assert.AreEqual(3, numbers.lowest());
		Assert.AreEqual(3, numbers.highest());
		Assert.AreEqual(3, numbers.certainValue());
		}
	}

[TestFixture]
public class TestInequalities
	{
	Puzzle puzzle;
	[TestFixtureSetUp]
	public void Init()
		{
		puzzle = new Puzzle(3);
		puzzle.setValue(0, 0, 1);
		puzzle.setValue(2, 2, 3);
		puzzle.process();
		}

	[Test]
	public void row0()
		{
		Assert.AreEqual(
			SprigFern.EqualityConstraint.Direction.Left,
			puzzle.validIneqRow(0, 0));
		Assert.AreEqual(
			SprigFern.EqualityConstraint.Direction.Right,
			puzzle.validIneqRow(0, 1));
		}

	[Test]
	public void row1()
		{
		Assert.AreEqual(
			SprigFern.EqualityConstraint.Direction.Right,
			puzzle.validIneqRow(1, 0));
		Assert.AreEqual(
			SprigFern.EqualityConstraint.Direction.Right,
			puzzle.validIneqRow(1, 1));
		}

	[Test]
	public void row2()
		{
		Assert.AreEqual(
			SprigFern.EqualityConstraint.Direction.Right,
			puzzle.validIneqRow(2, 0));
		Assert.AreEqual(
			SprigFern.EqualityConstraint.Direction.Left,
			puzzle.validIneqRow(2, 1));
		}

	[Test]
	public void cols()
		{
		Assert.AreEqual(
			SprigFern.EqualityConstraint.Direction.Left,
				puzzle.validIneqCol(0, 0));
		Assert.AreEqual(
			SprigFern.EqualityConstraint.Direction.Right,
				puzzle.validIneqCol(1, 0));
		Assert.AreEqual(
			SprigFern.EqualityConstraint.Direction.Right,
				puzzle.validIneqCol(2, 0));
		Assert.AreEqual(
			SprigFern.EqualityConstraint.Direction.Right,
				puzzle.validIneqCol(0, 1));
		Assert.AreEqual(
			SprigFern.EqualityConstraint.Direction.Right,
				puzzle.validIneqCol(1, 1));
		Assert.AreEqual(
			SprigFern.EqualityConstraint.Direction.Left,
				puzzle.validIneqCol(2, 1));
		}

	[Test]
	public void unknowns()
		{
		puzzle = new Puzzle(2);
		Assert.AreEqual(
			SprigFern.EqualityConstraint.Direction.None,
			puzzle.validIneqRow(0, 0));
		Assert.AreEqual(
			SprigFern.EqualityConstraint.Direction.None,
			puzzle.validIneqRow(1, 0));
		Assert.AreEqual(
			SprigFern.EqualityConstraint.Direction.None,
			puzzle.validIneqCol(0, 0));
		Assert.AreEqual(
			SprigFern.EqualityConstraint.Direction.None,
			puzzle.validIneqCol(1, 0));
		}
	}

[TestFixture]
public class TestPuzzle
	{
	Puzzle puzzle;
	[TestFixtureSetUp]
	public void Init()
		{
		this.puzzle = new Puzzle(3);
		}

	[SetUp]
	public void SetUp()
		{
		this.puzzle.reset();
		}

	private void checkSolution(uint[][] solution, uint size)
		{
		for(uint row=0; row<size; row++)
			for(uint col=0; col<size; col++)
				{
				uint[] values = this.puzzle.validValues(row, col);
				Assert.AreEqual(1, values.Length);
				Assert.AreEqual(solution[row][col], values[0]);
				}
		}

	[Test]
	public void Game1()
		{
		this.puzzle.setValue(0, 0, 1);
		this.puzzle.setValue(2, 2, 3);
		this.puzzle.process();
		uint[][] solution = new uint[][] {
			new uint[]{1,3,2},
			new uint[]{3,2,1},
			new uint[]{2,1,3}
		};
		this.checkSolution(solution, 3);
		}

	[Test]
	public void Game2()
		{
		this.puzzle.setValue(0, 0, 1);
		this.puzzle.setValue(1, 1, 2);
		this.puzzle.process();
		uint[][] solution = new uint[][] {
			new uint[]{1,3,2},
			new uint[]{3,2,1},
			new uint[]{2,1,3}
		};
		this.checkSolution(solution, 3);
		}
	}


[TestFixture]
public class Bugs
	{
	Puzzle puzzle;
	[SetUp]
	public void SetUp()
		{
		this.puzzle = new Puzzle(4);
		}

	[Test]
	public void UnableToSelectValidAlternative()
		{
		this.puzzle.setValue(0, 0, 1);
		this.puzzle.process();
		Assert.AreEqual(new uint[] { 1, 2, 3, 4 }, this.puzzle.validValues(0, 0));
		Assert.AreEqual(new uint[] {    2, 3, 4 }, this.puzzle.validValues(1, 0));
		Assert.AreEqual(new uint[] {    2, 3, 4 }, this.puzzle.validValues(2, 0));
		Assert.AreEqual(new uint[] {    2, 3, 4 }, this.puzzle.validValues(3, 0));
		
		this.puzzle.setValue(1, 0, 2);
		this.puzzle.process();
		Assert.AreEqual(new uint[] { 1,    3, 4 }, this.puzzle.validValues(0, 0));
		Assert.AreEqual(new uint[] {    2, 3, 4 }, this.puzzle.validValues(1, 0));
		Assert.AreEqual(new uint[] {       3, 4 }, this.puzzle.validValues(2, 0));
		Assert.AreEqual(new uint[] {       3, 4 }, this.puzzle.validValues(3, 0));

		this.puzzle.setValue(2, 0, 3);
		this.puzzle.process();
		Assert.AreEqual(new uint[] { 1,         }, this.puzzle.validValues(0, 0));
		Assert.AreEqual(new uint[] {    2,      }, this.puzzle.validValues(1, 0));
		Assert.AreEqual(new uint[] {       3,   }, this.puzzle.validValues(2, 0));
		Assert.AreEqual(new uint[] {          4 }, this.puzzle.validValues(3, 0));	

		// Although the Solver back-end allows cell (2,0) to be changed directly 
		// from '3' to '4', the calculation of '4' as the only possible value for
		// cell (3,0) causes it to get removed from the possible set of (2.0).
		// This prevents the GUI from selecting it, so the uintermediate stage
		// below is required. 
		this.puzzle.setValue(2, 0, 0);
		this.puzzle.process();
		Assert.AreEqual(new uint[] { 1,    3, 4 }, this.puzzle.validValues(0, 0));
		Assert.AreEqual(new uint[] {    2, 3, 4 }, this.puzzle.validValues(1, 0));
		Assert.AreEqual(new uint[] {       3, 4 }, this.puzzle.validValues(2, 0));
		Assert.AreEqual(new uint[] {       3, 4 }, this.puzzle.validValues(3, 0));

		this.puzzle.setValue(2, 0, 4);
		this.puzzle.process();
		Assert.AreEqual(new uint[] { 1,         }, this.puzzle.validValues(0, 0));
		Assert.AreEqual(new uint[] {    2,      }, this.puzzle.validValues(1, 0));
		Assert.AreEqual(new uint[] {       4,   }, this.puzzle.validValues(2, 0));
		Assert.AreEqual(new uint[] {          3 }, this.puzzle.validValues(3, 0));

		//int[] lstValids = this.puzzle.validValues(2, 0);
		//int[] lstExpected = new uint[] { 3, 4 };
		//Assert.AreEqual(lstExpected, lstValids);
		}

	[Test]
	public void IneqNotCatchingPossValsLowerThanActual()
		{
		this.puzzle.setValue(0, 0, 2);
		this.puzzle.process();
		Assert.AreEqual(new uint[] { 1, 2, 3, 4 }, this.puzzle.validValues(0, 0));
		Assert.AreEqual(new uint[] { 1, 3, 4 }, this.puzzle.validValues(1, 0));
		Assert.AreEqual(new uint[] { 1, 3, 4 }, this.puzzle.validValues(2, 0));
		Assert.AreEqual(new uint[] { 1, 3, 4 }, this.puzzle.validValues(3, 0));

		this.puzzle.setIneqRow(0, 0, EqualityConstraint.Direction.Left);
		Assert.AreEqual(
			EqualityConstraint.Direction.Left,
			this.puzzle.inequalities.rows[0, 0].actual
		);
		this.puzzle.process();

		Assert.AreEqual(new uint[] { 1, 2, 3    }, this.puzzle.validValues(0, 0));
		Assert.AreEqual(new uint[] {       3, 4 }, this.puzzle.validValues(1, 0));
		Assert.AreEqual(new uint[] { 1,    3, 4 }, this.puzzle.validValues(2, 0));
		Assert.AreEqual(new uint[] { 1,    3, 4 }, this.puzzle.validValues(3, 0));
		
		Assert.AreEqual(new uint[] { 1,    3, 4 }, this.puzzle.validValues(0, 1));
		Assert.AreEqual(new uint[] { 1, 2, 3, 4 }, this.puzzle.validValues(1, 1));
		Assert.AreEqual(new uint[] { 1, 2, 3, 4 }, this.puzzle.validValues(2, 1));
		Assert.AreEqual(new uint[] { 1, 2, 3, 4 }, this.puzzle.validValues(3, 1));

		Assert.AreEqual(new uint[] { 1,    3, 4 }, this.puzzle.validValues(0, 2));
		Assert.AreEqual(new uint[] { 1, 2, 3, 4 }, this.puzzle.validValues(1, 2));
		Assert.AreEqual(new uint[] { 1, 2, 3, 4 }, this.puzzle.validValues(2, 2));
		Assert.AreEqual(new uint[] { 1, 2, 3, 4 }, this.puzzle.validValues(3, 2));

		Assert.AreEqual(new uint[] { 1,    3, 4 }, this.puzzle.validValues(0, 3));
		Assert.AreEqual(new uint[] { 1, 2, 3, 4 }, this.puzzle.validValues(1, 3));
		Assert.AreEqual(new uint[] { 1, 2, 3, 4 }, this.puzzle.validValues(2, 3));
		Assert.AreEqual(new uint[] { 1, 2, 3, 4 }, this.puzzle.validValues(3, 3));
		}

	[Test]
	public void ExcludingPossibleInequals()
		{
		this.puzzle.setValue(0, 0, 2);
		this.puzzle.process();
		Assert.AreEqual(
			EqualityConstraint.Direction.None,
			this.puzzle.validIneqRow(0, 0)
		);
		}
	}
	
}


