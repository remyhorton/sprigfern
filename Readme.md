SprigFern: Futoshiki puzzle solver
==================================
SprigFern is a program to aid the creation and solving of _[Futoshiki][futo]_
number puzzles. In _Futoshiki_ game each number appears only once within every
row and column, and in addition some adjoining squares have to satisfy an
inequality between them. Number are cycled by clicking on the squares, an
d repeated clicking will only cycle through possible values for that square
, based on constraints from other squares. Squares that don't have an explicitly
set number, but only have one possible solution, are shown in grey. SprigFern
has been written using the GTK# tool-kit, and should run on any operating system
for which the [Mono][mono] run-time environment is available. It has been tested
using Mono for Slackware 14.2 and Ubuntu 12.04, as well as the .NET Framework v4
under Windows7.

![Screenshot](screenshot-10beta.png)


## Current status
SprigFern was released in March 2014, although since then it has not undergone
any further development. It was open-sourced in May 2018 when the project was
moved from its previous hosting onto Bitbucket, and in the process it underwent
a lot of cleanup (mostly removal of commented-out code and reconstructing the
project files). At the moment no further changes are planned, although in the
future I might rewrite the program using wxWidgets.


## Running SprigFern
Running SprigFern requires a platform capable of running .NET
[Common Language Runtime][clr] binaries, as well as having the GTK# run-time
libraries installed. Although notionally a .NET based program, SprigFern was
developed using MonoDevelop under Linux, so there should be no problems running
it on any non-Windows operating system.

Running using .NET run-time
: SprigFern requires .NET v4, which on Windows7 requires installation of the
[.NET run-time][net]. Windows8 includes .NET v4 as standard, but SprigFern has
not been tested on this operating system. SprigFern also requires the
[GTK# for .NET][gtk#] run-time libraries from Xamarin.

Running using Mono on Windows
: As an alternative to .NET, Windows users can install [Mono for Windows][mono-win].
This may be favourable in circumstances (e.g. no administrator privileges) where
installation of the .NET v4 run-time is undesirable or impossible. The only
complication is that Mono will not register itself as the execution environment
for .NET binaries in the same way the Java run-time is auto-associated with .jar
files. Therefore you need to either drag-and-drop the SprigFern binary onto
``mono.exe`` (or a shortcut to it), or specify SprigFern as a command-line paramete
r for Mono.

Running on Ubuntu 12.04
: Under Ubuntu 12.04 you need to install the ``mono-complete`` package, as a
bare-bones Mono install does not include libraries required to run SprigFern
. If you only have the basic libraries installed, you will likely get errors like
_Unhandled Exception: System.IO.FileNotFoundException: Could not load file or assembly_.


## The name _SprigFern_
The name SprigFern comes from the name of the Wellington pub where I first came
across the game and started to think about an algorithm for solving the puzzles,
and since used the name as a place-holder because I had forgotten the game's
actual name. By the time I got back online and found out the puzzle's actual name,
I decided to stick with this place-holder name.


## Licence
SprigFern is licenced under [version 2 of the GPL][gpl2].


## Contact
Send emails to ``remy.horton`` (at) ``gmail.com``


[futo]: http://en.wikipedia.org/wiki/Futoshiki
[mono]: http://www.mono-project.com/Main_Page
[clr]: http://en.wikipedia.org/wiki/Common_Language_Runtime
[net]: http://www.microsoft.com/en-gb/download/details.aspx?id=17718
[gtk#]: http://download.xamarin.com/GTKforWindows/Windows/gtk-sharp-2.12.21.msi
[mono-win]: http://www.go-mono.com/mono-downloads/download.html
[gpl2]: https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html

