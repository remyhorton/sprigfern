using System;
using Gtk;
using SprigFern;
using System.Collections.Generic;

public delegate void ProcessClickDelegate(object objSource, uint id, ButtonPressEventArgs argv);

public class NumberEventBox : Gtk.EventBox
{
	public NumberEventBox(int x) : base()
		{
		}
}

public class NumberFrame : Gtk.Frame
{
	private Gtk.Label labelText;
	private NumberEventBox boxEvents;
	private uint id;
	private ProcessClickDelegate funcProcessPress;
	public uint value;

	public NumberFrame(Gtk.Table tableParent, uint idFrame, uint posX, uint posY, ProcessClickDelegate funcClick, Pango.FontDescription ptrFontInfo) : base()
		{
		this.id = idFrame;
		this.funcProcessPress = funcClick;
		this.labelText = new Gtk.Label();
		//this.labelText.LabelProp = global::Mono.Unix.Catalog.GetString ("2");
		this.labelText.ModifyFont(ptrFontInfo);
		this.labelText.SetSizeRequest(32, 32);

		this.boxEvents = new NumberEventBox(55);
		this.boxEvents.BorderWidth = 0;
		this.boxEvents.Add(this.labelText);
		this.boxEvents.ButtonPressEvent += new Gtk.ButtonPressEventHandler(this.eventClick);
		this.Add(this.boxEvents);
		this.value = 0;

		tableParent.Add(this);
		Gtk.Table.TableChild ptrTableChild = (Gtk.Table.TableChild)tableParent[this];
		ptrTableChild.XOptions = ((global::Gtk.AttachOptions)(4));
		ptrTableChild.YOptions = ((global::Gtk.AttachOptions)(4));
		ptrTableChild.TopAttach = posY + 0;
		ptrTableChild.BottomAttach = posY + 1;
		ptrTableChild.LeftAttach = posX + 0;
		ptrTableChild.RightAttach = posX + 1;
		}

	private void eventClick(object objSource, ButtonPressEventArgs args)
		{
		Gdk.EventButton btnEvent = args.Event;
		if( btnEvent.Type == Gdk.EventType.TwoButtonPress ||
		    btnEvent.Type == Gdk.EventType.ThreeButtonPress )
			{
			return;
			}
		this.funcProcessPress(this, this.id, args);
		}

	public void setText(string strValue)
		{
		this.labelText.Text = strValue;
		}

	public void setDimmed(bool isFixed)
		{
		if( isFixed )
			this.labelText.ModifyFg(StateType.Normal, new Gdk.Color(128, 128, 128));
		else
			this.labelText.ModifyFg(StateType.Normal, new Gdk.Color(0, 0, 0));
		}
}


public class ConstraintFrame : Gtk.EventBox
	{
	private uint id;
	private Gtk.Image imgIcon;
	private ProcessClickDelegate funcProcessPress;
	public bool isLeftRight;
	public EqualityConstraint.Direction puzzleValue;

	public ConstraintFrame(Gtk.Table tableParent, uint idFrame, uint posX, uint posY, ProcessClickDelegate funcClick, bool isLeftRight, Gdk.Pixbuf ptrIconImg) : base()
		{
		this.id = idFrame;
		this.funcProcessPress = funcClick;
		this.imgIcon = new Gtk.Image();
		this.imgIcon.Pixbuf = ptrIconImg;
		this.Name = "eventBox";
		this.Add(this.imgIcon);
		this.ButtonPressEvent += new Gtk.ButtonPressEventHandler(this.eventClick);
		//this.ModifyBg(StateType.Normal, new Gdk.Color(0,255,0));
		this.isLeftRight = isLeftRight;
		this.puzzleValue = EqualityConstraint.Direction.None;

		tableParent.Add(this);
		Gtk.Table.TableChild ptrTableChild = (Gtk.Table.TableChild)tableParent[this];
		ptrTableChild.XOptions = ((global::Gtk.AttachOptions)(4));
		ptrTableChild.YOptions = ((global::Gtk.AttachOptions)(4));
		ptrTableChild.TopAttach = posY + 0;
		ptrTableChild.BottomAttach = posY + 1;
		ptrTableChild.LeftAttach = posX + 0;
		ptrTableChild.RightAttach = posX + 1;
		}

	private void eventClick(object objSource, ButtonPressEventArgs args)
		{
		Gdk.EventButton btnEvent = args.Event;
		if( btnEvent.Type == Gdk.EventType.TwoButtonPress ||
		   btnEvent.Type == Gdk.EventType.ThreeButtonPress )
			{
			return;
			}
		this.funcProcessPress(objSource, this.id, args);
		}
	public void setIcon(Gdk.Pixbuf ptrIconImg)
		{
		this.imgIcon.Pixbuf = ptrIconImg;
		}
	}


public partial class MainWindow: Gtk.Window
	{
	public MainWindow () : base (Gtk.WindowType.Toplevel)
		{
		this.loadGlyphs();
		this.puzzle = new Puzzle(5);
		this.makeGUI(5);
		this.DeleteEvent += this.OnDeleteEvent;
		}
	protected void OnDeleteEvent (object sender, DeleteEventArgs a)
		{
		Application.Quit ();
		a.RetVal = true;
		}

	private Puzzle puzzle;
	private List<NumberFrame> listNumberFrames;
	private List<ConstraintFrame> listConstraintFrames;
	private global::Gtk.Table tableMain;
	private Pango.FontDescription fontNumbers;

	Gdk.Pixbuf pcxUpGlyph;
	Gdk.Pixbuf pcxDownGlyph;
	Gdk.Pixbuf pcxLeftGlyph;
	Gdk.Pixbuf pcxRightGlyph;
	Gdk.Pixbuf pcxBlankGlyph;

	private Gtk.VBox boxMain = null;
	private Gtk.CheckMenuItem menuitemHint;

	private bool loadGlyphs()
		{
		System.Reflection.Assembly progAssembly = 
			System.Reflection.Assembly.GetExecutingAssembly();

		this.pcxUpGlyph = new Gdk.PixbufLoader(
			progAssembly.GetManifestResourceStream("SprigFern.up.png")
			).Pixbuf;
		this.pcxDownGlyph = new Gdk.PixbufLoader(
			progAssembly.GetManifestResourceStream("SprigFern.down.png")
		).Pixbuf;
		this.pcxLeftGlyph = new Gdk.PixbufLoader(
			progAssembly.GetManifestResourceStream("SprigFern.left.png")
			).Pixbuf;
		this.pcxRightGlyph = new Gdk.PixbufLoader(
			progAssembly.GetManifestResourceStream("SprigFern.right.png")
			).Pixbuf;
		this.pcxBlankGlyph = new Gdk.PixbufLoader(
			progAssembly.GetManifestResourceStream("SprigFern.none.png")
			).Pixbuf;

		return true;
		}


	protected MenuBar makeMenu()
		{
		MenuBar menu = new MenuBar();

		MenuItem menuFile = new MenuItem("File");
		Menu submenuFile = new Menu();

		MenuItem submenuNew = new MenuItem("New game..");
		submenuNew.Activated += this.OnMenuNew;
		submenuFile.Add(submenuNew);
		this.menuitemHint = new CheckMenuItem("Show hints");
		this.menuitemHint.Active = false;
		this.menuitemHint.Toggled += this.OnMenuHint;
		submenuFile.Add(this.menuitemHint);
		submenuFile.Add(new SeparatorMenuItem());
		MenuItem submenuExit = new MenuItem("Exit");
		submenuExit.Activated += this.OnMenuExit;
		submenuFile.Add(submenuExit);
		menuFile.Submenu = submenuFile;
		menu.Add(menuFile);

		MenuItem menuHelp= new MenuItem("Help");
		Menu submenuHelp = new Menu();
		MenuItem submenuAbout = new MenuItem("About");
		submenuAbout.Activated += this.OnMenuAbout;
		submenuHelp.Add(submenuAbout);
		menuHelp.Submenu = submenuHelp;
		menu.Add(menuHelp);

		return menu;
		}
	
	protected void OnMenuAbout(object obj, EventArgs args)
		{
		// days since jan 2000; seconds since midnight / 2
		Version asmVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
		Gtk.AboutDialog ab = new Gtk.AboutDialog();
		ab.ProgramName = "SprigFern Solver";
		ab.Version = "v1.1";
		ab.Comments = 
			string.Format(
				" v{0}.{1} (build {2}.{3}) \n\n", 
				asmVersion.Major, asmVersion.Minor,
			    asmVersion.Build, asmVersion.Revision) +
			"\n" +
			"\n" + 
			"\n" +
			"\n" +
			"\n" +
			"\n\n";

		ab.Logo = new Gdk.PixbufLoader(
			System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(
				"SprigFern.logo.png")
			).Pixbuf;

		ab.Copyright = "remy.horton@gmail.com";
		ab.Website = "https://bitbucket.org/remyhorton/sprigfern";
		ab.Run();
		ab.Destroy();
		}

	protected void OnMenuNew(object obj, EventArgs args)
		{
		NewDialog newDialog = new NewDialog();
		int valReturn = newDialog.Run();
		if(valReturn == 1)
			{
			int valNewSize = newDialog.spinPuzzleSize.ValueAsInt;
			this.puzzle = new Puzzle((uint)valNewSize);
			this.makeGUI((uint)valNewSize);
			}
		newDialog.Destroy();
		}

	protected void OnMenuHint(object obj, EventArgs args)
		{
		this.showPuzzle();
		}

	protected void OnMenuExit(object obj, EventArgs args)
		{
		Application.Quit();
		}

	protected void makeGUI(uint sizePuzzle)
		{
		uint nextNumberId = 0;
		uint nextIneqId = 0;
		this.fontNumbers = Pango.FontDescription.FromString("Arial 14");

		this.Name = "SprigFern";
		this.Title = "SprigFern Solver";
		this.WindowPosition = ((global::Gtk.WindowPosition)(4));
		this.Resizable = false;
		this.AllowGrow = false;

		uint cntCellsPerRow = (sizePuzzle << 1)-1;


		this.tableMain = new Gtk.Table(cntCellsPerRow, cntCellsPerRow, true);
		this.tableMain.BorderWidth = 6;
		this.tableMain.RowSpacing = 0;

		this.listNumberFrames = new List<NumberFrame>();
		this.listConstraintFrames = new List<ConstraintFrame>();

		for( uint posY=0; posY<sizePuzzle; posY++)
			{
			uint posTop = posY << 1;
			for( uint posX=0; posX<sizePuzzle; posX++)
				{
				uint posLeft = posX << 1;
				this.listNumberFrames.Add(
					new NumberFrame(this.tableMain, nextNumberId++, posLeft, posTop, this.click, fontNumbers)
				);
				}
			}
		for( uint posY=0; posY<cntCellsPerRow; posY++)
			{
			bool isLeftRight = (posY & 1) == 0;
			for( uint posX=(isLeftRight?1u:0u); posX<cntCellsPerRow; posX+=2)
				{
				this.listConstraintFrames.Add(
					new ConstraintFrame(this.tableMain, nextIneqId++, posX, posY, this.click, isLeftRight, this.pcxBlankGlyph)
				);
				}
			}

		if(this.boxMain != null)
			this.Remove(this.boxMain);
		this.boxMain =  new Gtk.VBox ();
		this.boxMain.Spacing = 6;
		this.boxMain.Add(this.makeMenu());
		this.boxMain.Add(this.tableMain);
		this.Add(this.boxMain);

		this.Child.ShowAll();
		this.DefaultWidth = 400;
		this.DefaultHeight = 400;
		this.Show ();
		}

	private void showPuzzle()
		{
		for(uint idxNumber=0; idxNumber<this.listNumberFrames.Count; idxNumber++)
			{
			NumberFrame ptrNumber = this.listNumberFrames[(int)idxNumber];
			if(ptrNumber.value != 0)
				{
				ptrNumber.setDimmed(false);
				ptrNumber.setText(ptrNumber.value.ToString());
				}
			else if(this.menuitemHint.Active == false)
				{
				ptrNumber.setText("");
				}
			else
				{
				uint posX = idxNumber % this.puzzle.Size;
				uint posY = idxNumber / this.puzzle.Size;
				uint[] lstValids = this.puzzle.validValues(posX, posY);
				if(lstValids.Length == 1 )
					{
					ptrNumber.setDimmed(true);
					ptrNumber.setText(lstValids[0].ToString());
					}
				else
					{
					ptrNumber.setText("");
					}
				}
			}

		for(int posX=0; posX<this.puzzle.Size; posX++)
			for(int posY=0; posY<this.puzzle.Size; posY++)
				{
				}
		}
	
	private void click(object objSource, uint id, ButtonPressEventArgs arg)
		{
		Type typeSource = objSource.GetType();
		if( typeSource == typeof(NumberFrame) )
			{
			NumberFrame ptrBox = (NumberFrame)objSource;
			uint posX = id % this.puzzle.Size;
			uint posY = id / this.puzzle.Size;

			// Have to calculate the validity sets with the current value
			// removed, because the current value may have to go elsewhere
			// to release alternative possibilities. 
			this.puzzle.setValue(posX, posY, 0);
			this.puzzle.process();

			uint[] lstValid = this.puzzle.validValues(posX, posY);
			uint valNew = 0;
			if(ptrBox.value == 0)
				{
				if( lstValid.Length == 0 )
					throw new SomethingBad("No possible values!");
				valNew = lstValid[0];
				}
			else
				{
				int idxValid = Array.IndexOf(lstValid, ptrBox.value);
				if( idxValid == -1 )
					throw new SomethingBad("Already-selected value invalid");
				if(idxValid + 1 == lstValid.Length)
					valNew = 0;
				else
					valNew = lstValid[idxValid + 1];
				}
			ptrBox.value = valNew;
			this.puzzle.setValue(posX, posY, valNew);
			this.puzzle.process();
			this.showPuzzle();
			}
		else if( typeSource == typeof(ConstraintFrame) )
			{
			ConstraintFrame ptrBox = (ConstraintFrame)objSource;
			EqualityConstraint.Direction valueOld = ptrBox.puzzleValue;
			EqualityConstraint.Direction valueNew = EqualityConstraint.Direction.None;
			EqualityConstraint.Direction valuePoss = EqualityConstraint.Direction.None;
			uint posCol = id % ((this.puzzle.Size<<1)-1);
			uint posRow = id / ((this.puzzle.Size<<1)-1);

			if(ptrBox.isLeftRight == false)
				posCol = posCol - (this.puzzle.Size - 1);

			// Calculation of possible inequals needs absence of current one, otherwise
			// knock-on possibility sets might exclude possible replacement values. A
			// simular issue was present with number selection.
			if(ptrBox.isLeftRight)
				this.puzzle.setIneqRow(posRow, posCol, EqualityConstraint.Direction.None);
			else
				this.puzzle.setIneqCol(posCol, posRow, EqualityConstraint.Direction.None);
			this.puzzle.process();

			if(ptrBox.isLeftRight)
				valuePoss = this.puzzle.validIneqRow(posRow,posCol);
			else
				valuePoss = this.puzzle.validIneqCol(posCol,posRow);

			if(valuePoss == EqualityConstraint.Direction.None)
				{
				// No constraint. Toggle between Left/Right/None
				if(valueOld == EqualityConstraint.Direction.Left)
					valueNew = EqualityConstraint.Direction.Right;
				else if(valueOld == EqualityConstraint.Direction.Right)
					valueNew = EqualityConstraint.Direction.None;
				else
					valueNew = EqualityConstraint.Direction.Left;
				}
			else if(valueOld == valuePoss)
				valueNew = EqualityConstraint.Direction.None;
			else
				valueNew = valuePoss;
		
			// Redraw
			ptrBox.puzzleValue = valueNew;
			if(ptrBox.isLeftRight)
				{
				this.puzzle.setIneqRow(posRow, posCol, valueNew);
				}
			else
				{
				this.puzzle.setIneqCol(posCol, posRow, valueNew);
				}
			this.puzzle.process();
			this.showPuzzle();


			if(ptrBox.isLeftRight)
				{
				switch( valueNew )
					{
					case EqualityConstraint.Direction.Left:
						ptrBox.setIcon(this.pcxLeftGlyph);
						break;
					case EqualityConstraint.Direction.Right:
						ptrBox.setIcon(this.pcxRightGlyph);
						break;
					default:
						ptrBox.setIcon(this.pcxBlankGlyph);
						break;
					}
				}
			else
				{
				switch( valueNew )
					{
					case EqualityConstraint.Direction.Left:
						ptrBox.setIcon(this.pcxUpGlyph);
						break;
						case EqualityConstraint.Direction.Right:
						ptrBox.setIcon(this.pcxDownGlyph);
						break;
						default:
						ptrBox.setIcon(this.pcxBlankGlyph);
						break;
					}
				}
			}
		else 
			throw new ArgumentException();
		this.showPuzzle();
		}

	protected void eventClick (object o, ButtonPressEventArgs args)
		{
		throw new NotImplementedException ();
		}

	protected void eventPress (object o, ButtonPressEventArgs args)
		{
		throw new NotImplementedException ();
		}
	}

public class NewDialog : Gtk.Dialog
	{
	// This is cut'n'pasted auto-generated code, minimally edited to remove 
	// the Mono.Unix references that screw up under .NET on windows. 
	private Gtk.HBox hbox1;
	public  Gtk.SpinButton spinPuzzleSize;
	private Gtk.Label label;
	private Gtk.Button buttonCancel;
	private Gtk.Button buttonOk;
	public NewDialog()
		{
		this.Name = "SprigFern.NewDialog";
		this.Title = "New Game";
		this.WindowPosition = ((global::Gtk.WindowPosition)(4));
		this.Modal = true;
		this.Resizable = false;
		this.DestroyWithParent = true;

		// Internal child SprigFern.NewDialog.VBox
		global::Gtk.VBox w1 = this.VBox;
		w1.Name = "dialog1_VBox";
		w1.BorderWidth = ((uint)(2));

		// Container child dialog1_VBox.Gtk.Box+BoxChild
		this.hbox1 = new global::Gtk.HBox ();
		this.hbox1.Name = "hbox1";
		this.hbox1.Spacing = 6;

		// Container child hbox1.Gtk.Box+BoxChild
		this.spinPuzzleSize = new global::Gtk.SpinButton (4, 20, 1);
		this.spinPuzzleSize.CanFocus = true;
		this.spinPuzzleSize.Name = "spinbutton2";
		this.spinPuzzleSize.Adjustment.PageIncrement = 10;
		this.spinPuzzleSize.ClimbRate = 1;
		this.spinPuzzleSize.Numeric = true;
		this.spinPuzzleSize.Value = 5;
		this.hbox1.Add (this.spinPuzzleSize);
		global::Gtk.Box.BoxChild w2 = ((global::Gtk.Box.BoxChild)(this.hbox1 [this.spinPuzzleSize]));
		w2.Position = 0;
		w2.Expand = false;
		w2.Fill = false;

		// Container child hbox1.Gtk.Box+BoxChild
		this.label = new global::Gtk.Label ();
		this.label.Name = "label";
		this.label.LabelProp = "Puzzle size";
		this.hbox1.Add (this.label);
		global::Gtk.Box.BoxChild w3 = ((global::Gtk.Box.BoxChild)(this.hbox1 [this.label]));
		w3.Position = 1;
		w3.Expand = false;
		w3.Fill = false;
		w1.Add (this.hbox1);
		global::Gtk.Box.BoxChild w4 = ((global::Gtk.Box.BoxChild)(w1 [this.hbox1]));
		w4.Position = 0;
		w4.Expand = false;
		w4.Fill = false;

		// Internal child SprigFern.NewDialog.ActionArea
		global::Gtk.HButtonBox w5 = this.ActionArea;
		w5.Name = "dialog1_ActionArea";
		w5.Spacing = 10;
		w5.BorderWidth = ((uint)(5));
		w5.LayoutStyle = ((global::Gtk.ButtonBoxStyle)(4));

		// Container child dialog1_ActionArea.Gtk.ButtonBox+ButtonBoxChild
		this.buttonCancel = new global::Gtk.Button ();
		this.buttonCancel.CanDefault = true;
		this.buttonCancel.CanFocus = true;
		this.buttonCancel.Name = "buttonCancel";
		this.buttonCancel.UseStock = true;
		this.buttonCancel.UseUnderline = true;
		this.buttonCancel.Label = "gtk-cancel";
		this.AddActionWidget (this.buttonCancel, 0);
		global::Gtk.ButtonBox.ButtonBoxChild w6 = ((global::Gtk.ButtonBox.ButtonBoxChild)(w5 [this.buttonCancel]));
		w6.Expand = false;
		w6.Fill = false;

		// Container child dialog1_ActionArea.Gtk.ButtonBox+ButtonBoxChild
		this.buttonOk = new global::Gtk.Button ();
		this.buttonOk.CanDefault = true;
		this.buttonOk.CanFocus = true;
		this.buttonOk.Name = "buttonOk";
		this.buttonOk.UseStock = true;
		this.buttonOk.UseUnderline = true;
		this.buttonOk.Label = "gtk-ok";
		this.AddActionWidget (this.buttonOk, 1);
		global::Gtk.ButtonBox.ButtonBoxChild w7 = ((global::Gtk.ButtonBox.ButtonBoxChild)(w5 [this.buttonOk]));
		w7.Position = 1;
		w7.Expand = false;
		w7.Fill = false;
		if ((this.Child != null)) {
			this.Child.ShowAll ();
		}
		this.DefaultWidth = 400;
		this.DefaultHeight = 300;
		this.Show ();
	}
}
