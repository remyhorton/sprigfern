﻿/*
 * Created by SharpDevelop.
 * User: Remy
 * Date: 10/01/2014
 * Time: 22:21-23:51 (1hr 30mins) 01:51
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Text;
using System.Collections.Generic;


namespace SprigFern
{
class SomethingBad : Exception
	{
	string strReason;
	public SomethingBad(string reason)
		{
		this.strReason = reason;
		}
	public string Reason
		{
		get
			{
			return this.strReason;
			}
		}
	}

class NumberConstraint
	{
	private List<uint> possibleValues;
	private uint actualValue;
	private uint maxValue;
	
	public NumberConstraint(uint maxValue)
		{
		this.possibleValues = new List<uint>();
		this.maxValue = maxValue;
		this.reset();
		}
	
	public void reset()
		{
		this.possibleValues.Clear();
		for(uint value=1; value<=this.maxValue; value++)
			this.possibleValues.Add(value);
		}
	
	public override string ToString()
		{
		StringBuilder sbOut = new StringBuilder();
		uint count = 0;
		if( this.actualValue != 0 )
			{
			sbOut.AppendFormat("{0}",this.actualValue+5);
			count++;
			}
		else
			foreach( uint value in possibleValues )
			{
			sbOut.AppendFormat("{0}",value);
			count++;
			}
		while( count++ < this.maxValue )
			sbOut.Append(" ");
		return sbOut.ToString();
		}
	
	/// <summary>
	/// Remove the specified value.
	/// </summary>
	/// <param name="value">Value.</param>
	/// <returns>Whether number was initially present</returns>
	public bool remove(uint value)
		{
		return this.possibleValues.Remove(value);
		//if( this.possibleValues.Count == 0 )
		//	return false;
		//return true;
		}
	
	public void set(uint value)
		{
		if( value != 0 && ! this.possibleValues.Exists(maxValue => maxValue == value) )
			throw new SomethingBad("Tried to set value not in possible set");
		this.actualValue = value;
		}
	
	public uint certainValue()
		{
		if( this.actualValue != 0 )
			return this.actualValue;
		if( this.possibleValues.Count == 1 )
			return this.possibleValues[0];
		return 0;
		}
	
	public uint lowest()
		{
		if( this.actualValue != 0 )
			return this.actualValue;
		if( this.possibleValues.Count != 0 )
			{
			uint valLow = this.possibleValues[0];
			foreach( uint value in this.possibleValues )
				if( valLow > value )
					valLow = value;
			return valLow;
			}
		return 0;
		}
	
	public uint highest()
		{
		if( this.actualValue != 0 )
			return this.actualValue;
		uint valHigh = 0;
		if( this.possibleValues.Count != 0 )
			foreach( uint value in this.possibleValues )
				if( valHigh < value )
					valHigh = value;
		return valHigh;
		}

	public uint[] possible()
		{
		return this.possibleValues.ToArray();
		}
	}

public class EqualityConstraint
	{
	public enum Direction
		{
		None,
		Left,
		Right
		};
	public Direction actual;
	public Direction forced;
	public EqualityConstraint()
		{
		this.actual = Direction.None;
		this.forced = Direction.None;
		}
	}

class Inequalities
	{
	public EqualityConstraint[,] rows;
	public EqualityConstraint[,] cols;
	
	public Inequalities(uint maxValue)
		{
		this.rows = new EqualityConstraint[maxValue,maxValue-1];
		this.cols = new EqualityConstraint[maxValue,maxValue-1];
		for( uint idxRow=0; idxRow<maxValue; idxRow++ )
			{
			for(uint idxCol=0; idxCol<maxValue-1; idxCol++ )
				{
				this.rows[idxRow,idxCol] = new EqualityConstraint();
				this.cols[idxRow,idxCol] = new EqualityConstraint();
				}
			}
		}
	}

class Puzzle
	{
	public NumberConstraint[,] numbers;
	public Inequalities  inequalities; // FIXME
	private uint maxValue;
	
	public Puzzle(uint maxValue)
		{
		this.inequalities = new Inequalities(maxValue);
		this.numbers = new NumberConstraint[maxValue,maxValue];
		for(uint x=0; x<maxValue; x++)
			for(uint y=0; y<maxValue; y++)
				{
				this.numbers[x,y] = new NumberConstraint(maxValue);
				//this.numbers[x,y].reset();
				}
		this.maxValue = maxValue;
		}


	public uint Size
		{
		get
			{
			return (uint)this.maxValue;
			}
		}
	
	private bool eliminate(uint posX, uint posY, uint value)
		{
		bool changed = false;
		for(uint x=0; x<this.maxValue; x++)
			if( x != posX )
				changed = changed || this.numbers[x,posY].remove(value);
		for(uint y=0; y<this.maxValue; y++)
			if( y != posY )
				changed = changed || this.numbers[posX,y].remove(value);			
		return changed;
		}
	
	/// <summary>
	/// Eliminates cell value from all others in same row/column
	/// </summary>
	/// <param name="idxCol">Column of reference cell</param>
	/// <param name="idxRow">Row of reference cell</param>
	/// <returns>Whether elimination changed anything</returns>
	private bool processCell(uint idxCol, uint idxRow)
		{
		bool changes = false;
		uint value = this.numbers[idxCol,idxRow].certainValue();
		if( value != 0 )
			{
			for(uint x=0; x<this.maxValue; x++)
				if( x != idxCol )
					changes |= this.numbers[x,idxRow].remove(value);
			for(uint y=0; y<this.maxValue; y++)
				if( y != idxRow )
					changes |= this.numbers[idxCol,y].remove(value);
			}
		return changes; 
		}
	
	private bool processRow(uint idxRow)
		{
		bool changes = false;
		for(uint idxCol=0; idxCol<this.maxValue; idxCol++)
			changes |= this.processCell(idxCol,idxRow);
		for(uint idxCol=0; idxCol<this.maxValue-1; idxCol++)
			{
			uint valCell;
			switch( this.inequalities.rows[idxRow,idxCol].actual )
				{
				case EqualityConstraint.Direction.Left:
					valCell = this.numbers[idxCol, idxRow].lowest();
					changes = changes || this.numbers[idxCol + 1, idxRow].remove(valCell);
					valCell = this.numbers[idxCol + 1, idxRow].highest();
					changes = changes || this.numbers[idxCol, idxRow].remove(valCell);
					valCell = this.numbers[idxCol, idxRow].certainValue();
					if(valCell != 0)
						{
						for(uint valKill=1; valKill<valCell; valKill++)
							changes = changes || this.numbers[idxCol + 1, idxRow].remove(valKill);
						}
					valCell = this.numbers[idxCol+1, idxRow].certainValue();
					if(valCell != 0)
						{
						for(uint valKill=valCell+1; valKill<=this.maxValue; valKill++)
							changes = changes || this.numbers[idxCol, idxRow].remove(valKill);
						}
					break;
				case EqualityConstraint.Direction.Right:
					valCell = this.numbers[idxCol,idxRow].highest();
					changes = changes || this.numbers[idxCol+1,idxRow].remove(valCell);
					valCell = this.numbers[idxCol+1,idxRow].lowest();
					changes = changes || this.numbers[idxCol,idxRow].remove(valCell);
					valCell = this.numbers[idxCol, idxRow].certainValue();
					if(valCell != 0)
						{
						for(uint valKill=valCell+1; valKill<=this.maxValue; valKill++)
							changes = changes || this.numbers[idxCol + 1, idxRow].remove(valKill);
						}
					valCell = this.numbers[idxCol+1, idxRow].certainValue();
					if(valCell != 0)
						{
						for(uint valKill=1; valKill<valCell; valKill++)
							changes = changes || this.numbers[idxCol, idxRow].remove(valKill);
						}
					break;								
				}
			}
		return changes;
		}
	
	private bool processCol(uint idxCol)
		{
		bool changes = false;
		for(uint idxRow=0; idxRow<this.maxValue; idxRow++)
			{
			uint value = this.numbers[idxCol,idxRow].certainValue();
			if( value != 0 )
				changes = changes || this.eliminate(idxCol,idxRow,value);
			}
		for(uint idxRow=0; idxRow<this.maxValue-1; idxRow++)
			{
			uint valCell;
			switch( this.inequalities.cols[idxCol,idxRow].actual )
				{
				case EqualityConstraint.Direction.Left:
					valCell = this.numbers[idxCol,idxRow].lowest();
					changes = changes || this.numbers[idxCol,idxRow+1].remove(valCell);
					valCell = this.numbers[idxCol,idxRow+1].highest();
					changes = changes || this.numbers[idxCol,idxRow].remove(valCell);

					valCell = this.numbers[idxCol, idxRow].certainValue();
					if(valCell != 0)
						{
						for(uint valKill=1; valKill<valCell; valKill++)
							changes = changes || this.numbers[idxCol, idxRow+1].remove(valKill);
						}
					valCell = this.numbers[idxCol, idxRow+1].certainValue();
					if(valCell != 0)
						{
						for(uint valKill=valCell+1; valKill<=this.maxValue; valKill++)
							changes = changes || this.numbers[idxCol, idxRow].remove(valKill);
						}
					break;
				case EqualityConstraint.Direction.Right:
					valCell = this.numbers[idxCol,idxRow].highest();
					changes = changes || this.numbers[idxCol,idxRow+1].remove(valCell);
					valCell = this.numbers[idxCol,idxRow+1].lowest();
					changes = changes || this.numbers[idxCol,idxRow].remove(valCell);
					if(valCell != 0)
						{
						for(uint valKill=valCell+1; valKill<=this.maxValue; valKill++)
							changes = changes || this.numbers[idxCol, idxRow+1].remove(valKill);
						}
					valCell = this.numbers[idxCol, idxRow+1].certainValue();
					if(valCell != 0)
						{
						for(uint valKill=1; valKill<valCell; valKill++)
							changes = changes || this.numbers[idxCol, idxRow].remove(valKill);
						}
					break;								
				}
			}
		return changes;			
		}
	
	public void process()
		{
		for(uint x=0; x<maxValue; x++)
			for(uint y=0; y<maxValue; y++)
				this.numbers[x,y].reset();
		bool changes = true;
		while( changes )
			{
			changes = false;
			for(uint idxRow=0; idxRow<this.maxValue; idxRow++)
				{
				changes = changes || this.processRow(idxRow);
				changes = changes || this.processCol(idxRow);
				}
			}
		}

	public bool setValue(uint x, uint y, uint value)
		{
		this.numbers[x,y].set(value);
		return false;
		}
	
	public void setIneqRow(uint idxRow, uint idxCell, EqualityConstraint.Direction value)
		{
		this.inequalities.rows[idxRow,idxCell].actual = value;
		}

	public void setIneqCol(uint idxCol, uint idxCell, EqualityConstraint.Direction value)
		{
		this.inequalities.cols[idxCol,idxCell].actual = value;
		}

	public uint[] validValues(uint x, uint y)
		{
		return this.numbers[x, y].possible();
		//return new List<int>( new int[]{1,2,3});
		}

	public EqualityConstraint.Direction validIneqRow(uint idxRow, uint idxCell)
		{
		if(this.numbers[idxCell, idxRow].highest() < this.numbers[idxCell + 1, idxRow].lowest())
			return EqualityConstraint.Direction.Left;
		if(this.numbers[idxCell, idxRow].lowest()  > this.numbers[idxCell + 1, idxRow].highest())
			return EqualityConstraint.Direction.Right;
		return EqualityConstraint.Direction.None;
		}

	public EqualityConstraint.Direction validIneqCol(uint idxCol, uint idxCell)
		{
		if(this.numbers[idxCol, idxCell].highest() < this.numbers[idxCol, idxCell+1].lowest())
			return EqualityConstraint.Direction.Left;
		if(this.numbers[idxCol, idxCell].lowest()  > this.numbers[idxCol, idxCell+1].highest())
			return EqualityConstraint.Direction.Right;
		return EqualityConstraint.Direction.None;
		}

			
	public void print()
		{
		System.Text.StringBuilder sbResult = new System.Text.StringBuilder();
		System.Text.StringBuilder sbDivide = new System.Text.StringBuilder();
		for(int cnt=0; cnt<this.maxValue-2; cnt++)
			sbDivide.Append("-");
		
		sbResult.Append("+");
		for(int x=0; x<maxValue; x++)
			{
			for(int cnt=0; cnt<this.maxValue; cnt++)
				sbResult.Append("-");
			sbResult.Append("+");
			}
		
		sbResult.Append("\n");
		for(int y=0; y<maxValue; y++)
			{
			sbResult.Append("|");
			for(int x=0; x<maxValue; x++)
				{
				sbResult.Append( this.numbers[x,y].ToString() );
				if( x < this.maxValue-1) 
					{
					if( this.inequalities.rows[y,x].actual == EqualityConstraint.Direction.Left )
						sbResult.Append("<");
					else if( this.inequalities.rows[y,x].actual == EqualityConstraint.Direction.Right )
						sbResult.Append(">");
					else
						sbResult.Append("|");
					}
				else
					sbResult.Append("|");
				}
			sbResult.Append("\n+");
			if( y < this.maxValue-1 )
				for(int x=0; x<maxValue; x++)
					{
					sbResult.Append("-");
					if( this.inequalities.cols[x,y].actual == EqualityConstraint.Direction.Left )
						sbResult.Append("^");
					else if( this.inequalities.cols[x,y].actual == EqualityConstraint.Direction.Right )
						sbResult.Append(">");
					else
						sbResult.Append("-");
					sbResult.Append(sbDivide);
					sbResult.Append("+");
					}
			else
				for(int x=0; x<maxValue; x++)
					{
					sbResult.Append("-");
					sbResult.Append(sbDivide);
					sbResult.Append("-+");
					}
			sbResult.Append("\n");
			
			}
		sbResult.Append("\n");
		Console.Write( sbResult.ToString() );
		}

	public void reset()
		{
		for(uint x=0; x<maxValue; x++)
			for(uint y=0; y<maxValue; y++)
				this.numbers[x,y].reset();
		for(uint x=0; x<maxValue; x++)
			for(uint y=0; y<maxValue-1; y++)
				{
				this.setIneqRow(x, y, EqualityConstraint.Direction.None);
				this.setIneqCol(x, y, EqualityConstraint.Direction.None);
				}
		}
	}
}
